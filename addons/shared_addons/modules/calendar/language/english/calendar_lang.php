<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['calendar:post']                 = 'Post';
$lang['calendar:posts']                   = 'Posts';

// labels
$lang['calendar:posted_label']                   = 'Posted';
$lang['calendar:posted_label_alt']               = 'Posted at';
$lang['calendar:written_by_label']				= 'Written by';
$lang['calendar:author_unknown']				= 'Unknown';
$lang['calendar:keywords_label']				= 'Keywords';
$lang['calendar:tagged_label']					= 'Tagged';
$lang['calendar:description_label']              = 'Description';
$lang['calendar:calendar_label']                 = 'Calendar';
$lang['calendar:date_label']                     = 'Date';
$lang['calendar:date_at']                        = 'at';
$lang['calendar:time_label']                     = 'Time';
$lang['calendar:status_label']                   = 'Status';
$lang['calendar:draft_label']                    = 'Draft';
$lang['calendar:live_label']                     = 'Live';
$lang['calendar:content_label']                  = 'Content';
$lang['calendar:options_label']                  = 'Options';
$lang['calendar:intro_label']                    = 'Introduction';
$lang['calendar:no_category_select_label']       = '-- None --';
$lang['calendar:new_category_label']             = 'Add a category';
$lang['calendar:subscripe_to_rss_label']         = 'Subscribe to RSS';
$lang['calendar:all_posts_label']             = 'All posts';
$lang['calendar:posts_of_category_suffix']    = ' posts';
$lang['calendar:rss_name_suffix']                = ' Calendar';
$lang['calendar:rss_category_suffix']            = ' Calendar';
$lang['calendar:author_name_label']              = 'Author name';
$lang['calendar:read_more_label']                = 'Read More&nbsp;&raquo;';
$lang['calendar:created_hour']                   = 'Created on Hour';
$lang['calendar:created_minute']                 = 'Created on Minute';
$lang['calendar:comments_enabled_label']         = 'Comments Enabled';

// titles
$lang['calendar:create_title']                   = 'Add Post';
$lang['calendar:edit_title']                     = 'Edit post "%s"';
$lang['calendar:archive_title']                 = 'Archive';
$lang['calendar:posts_title']					= 'Posts';
$lang['calendar:rss_posts_title']				= 'Calendar posts for %s';
$lang['calendar:blog_title']					= 'Calendar';
$lang['calendar:list_title']					= 'List Posts';

// messages
$lang['calendar:no_posts']                    = 'There are no posts.';
$lang['calendar:subscripe_to_rss_desc']          = 'Get posts straight away by subscribing to our RSS feed. You can do this via most popular e-mail clients, or try <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['calendar:currently_no_posts']          = 'There are no posts at the moment.';
$lang['calendar:post_add_success']            = 'The post "%s" was added.';
$lang['calendar:post_add_error']              = 'An error occured.';
$lang['calendar:edit_success']                   = 'The post "%s" was updated.';
$lang['calendar:edit_error']                     = 'An error occurred.';
$lang['calendar:publish_success']                = 'The post "%s" has been published.';
$lang['calendar:mass_publish_success']           = 'The posts "%s" have been published.';
$lang['calendar:publish_error']                  = 'No posts were published.';
$lang['calendar:delete_success']                 = 'The post "%s" has been deleted.';
$lang['calendar:mass_delete_success']            = 'The posts "%s" have been deleted.';
$lang['calendar:delete_error']                   = 'No posts were deleted.';
$lang['calendar:already_exist_error']            = 'An post with this URL already exists.';

$lang['calendar:twitter_posted']                 = 'Posted "%s" %s';
$lang['calendar:twitter_error']                  = 'Twitter Error';

// date
$lang['calendar:archive_date_format']		= "%B %Y";