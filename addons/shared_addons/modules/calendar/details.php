<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * calendar module
 *
 * @author PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\calendar
 */
class Module_Calendar extends Module {

	public $version = '1.0';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'Calendar',
				'ar' => 'المدوّنة',
				'br' => 'calendar',
				'pt' => 'calendar',
				'el' => 'Ιστολόγιο',
				'he' => 'בלוג',
				'id' => 'calendar',
				'lt' => 'Blogas',
				'pl' => 'calendar',
				'ru' => 'Блог',
				'zh' => '文章',
				'hu' => 'calendar',
				'fi' => 'Blogi',
				'th' => 'บล็อก',
            	'se' => 'Blogg',
			),
			'description' => array(
				'en' => 'Add calendars with images.',
				'ar' => 'أنشر المقالات على مدوّنتك.',
				'br' => 'Escrever publicações de calendar',
				'pt' => 'Escrever e editar publicações no calendar',
				'cs' => 'Publikujte nové články a příspěvky na calendar.', #update translation
				'da' => 'Skriv blogindlæg',
				'de' => 'Veröffentliche neue Artikel und calendar-Einträge', #update translation
				'sl' => 'Objavite calendar prispevke',
				'fi' => 'Kirjoita blogi artikkeleita.',
				'el' => 'Δημιουργήστε άρθρα και εγγραφές στο ιστολόγιο σας.',
				'es' => 'Escribe entradas para los artículos y calendar (web log).', #update translation
				'fi' => 'Kirjoita blogi artikkeleita.',
				'fr' => 'Envoyez de nouveaux posts et messages de calendar.', #update translation
				'he' => 'ניהול בלוג',
				'id' => 'Post entri calendar',
				'it' => 'Pubblica notizie e post per il calendar.', #update translation
				'lt' => 'Rašykite naujienas bei calendar\'o įrašus.',
				'nl' => 'Post nieuwsartikelen en blogs op uw site.',
				'pl' => 'Dodawaj nowe wpisy na blogu',
				'ru' => 'Управление записями блога.',
				'sl' => 'Objavite calendar prispevke',
				'zh' => '發表新聞訊息、部落格等文章。',
				'th' => 'โพสต์รายการบล็อก',
	            'hu' => 'calendar bejegyzések létrehozása.',
	            'se' => 'Inlägg i bloggen.',
			),
			'frontend'	=> true,
			'backend'	=> true,
			'skip_xss'	=> true,
			'menu'		=> 'content',

			'roles' => array(
				'put_live', 'edit_live', 'delete_live'
			),

			'sections' => array(
			    'calendar' => array(
				    'name' => 'calendar:calendar_title',
				    'uri' => 'admin/calendar',
				    'shortcuts' => array(
						array(
					 	   'name' => 'calendar:create_title',
						    'uri' => 'admin/calendar/create',
						    'class' => 'add'
						),
					),
				),
		    ),
		);
	}

	public function install()
	{
		$this->dbforge->drop_table('calendar');
		$this->dbforge->drop_table('images');
		$this->dbforge->drop_table('calendar_images');

		$tables = array(
			'calendars' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
				'title' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false, 'unique' => true),
				'description' => array('type' => 'TEXT'),
				'keywords' => array('type' => 'VARCHAR', 'constraint' => 32, 'default' => ''),
				'created_on' => array('type' => 'INT', 'constraint' => 11),
				'updated_on' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
			),
			'images' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
				'original' => array('type' => 'VARCHAR', 'constraint' => 160, 'null' => false, 'unique' => true, 'key' => true),
				'medium' => array('type' => 'VARCHAR', 'constraint' => 160, 'null' => false, 'unique' => true, 'key' => true),
				'thumb' => array('type' => 'VARCHAR', 'constraint' => 160, 'null' => false, 'unique' => true),
			),
			'calendars_images' => array(
				'calendar_id' => array('type' => 'INT', 'constraint' => 11, 'key' => true),
				'month' => array('type' => 'INT', 'constraint' => 2, 'key' => true),
				'image_id' => array('type' => 'INT', 'constraint' => 11, 'key' => true),
				'votes' => array('type' => 'INT', 'constraint' => 11),
			),
			'votes' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
				'user_id' => array('type' => 'INT', 'constraint' => 11, 'key' => true),
				'calendar_id' => array('type' => 'INT', 'constraint' => 11, 'key' => true),
				'month' => array('type' => 'INT', 'constraint' => 2, 'key' => true),
				'image_id' => array('type' => 'INT', 'constraint' => 11, 'key' => true),
				'rating' => array('type' => 'INT', 'constraint' => 1),
			),
		);

		return $this->install_tables($tables);
	}

	public function uninstall()
	{
		// This is a core module, lets keep it around.
		return false;
	}

	public function upgrade($old_version)
	{
		return true;
	}
}
