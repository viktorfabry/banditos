<?php if ($calendar) : ?>
	<table border="0" class="table-list">
		<thead>
			<tr>
				<th width="20"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all')); ?></th>
				<th><?php echo lang('calendar:calendar_label'); ?></th>
				<th class="collapse"><?php echo lang('calendar:decription_label'); ?></th>
				<th class="collapse"><?php echo lang('calendar:date_label'); ?></th>
				<th class="collapse"><?php echo lang('calendar:written_by_label'); ?></th>
				<th><?php echo lang('calendar:status_label'); ?></th>
				<th width="180"></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="7">
					<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php foreach ($calendar as $cal) : ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $cal->id); ?></td>
					<td><?php echo $cal->title; ?></td>
					<td class="collapse"><?php echo $cal->description; ?></td>
					<td class="collapse"><?php echo format_date($cal->created_on); ?></td>
					<td class="collapse">
					<?php if (isset($cal->display_name)): ?>
						<?php echo anchor('user/' . $cal->author_id, $cal->display_name, 'target="_blank"'); ?>
					<?php else: ?>
						<?php echo lang('calendar:author_unknown'); ?>
					<?php endif; ?>
					</td>
					<td><?php //echo lang('calendar:'.$cal->status.'_label'); ?></td>
					<td>

                        <?php echo anchor('calendar/' . $cal->id, lang('global:view'), 'class="btn green" target="_blank"');?>
						<?php echo anchor('admin/calendar/edit/' . $cal->id, lang('global:edit'), 'class="btn orange edit"'); ?>
						<?php echo anchor('admin/calendar/delete/' . $cal->id, lang('global:delete'), array('class'=>'confirm btn red delete')); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php else: ?>
	<div class="no_data"><?php echo lang('calendar:currently_no_posts'); ?></div>
<?php endif; ?>