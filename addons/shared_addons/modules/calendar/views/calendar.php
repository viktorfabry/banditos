<?php if (isset($category->title)): ?>
	<h2 id="page_title"><?php echo $category->title; ?></h2>

<?php elseif (isset($tag)): ?>
	<h2 id="page_title"><?php echo lang('calendar:tagged_label').': '.$tag; ?></h2>

<?php endif; ?>

<?php if ( ! empty($calendar)): ?>
<?php foreach ($calendar as $cal): ?>
	<div class="cal">
		<!-- cal heading -->
		<h3><?php echo  anchor('calendar/'.$cal->id, $cal->title); ?></h3>
		
		<div class="meta">
			<div class="date">
				<?php echo lang('calendar:posted_label');?>: 
				<span><?php echo format_date($cal->created_on); ?></span>
			</div>
			
			<?php if ($cal->keywords): ?>
			<div class="keywords">
				<?php echo lang('calendar:tagged_label');?>:
				<?php foreach ($cal->keywords as $keyword): ?>
					<span><?php echo anchor('blog/tagged/'.$keyword->name, $keyword->name, 'class="keyword"') ?></span>
				<?php endforeach; ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
<?php endforeach; ?>

<?php echo $pagination['links']; ?>

<?php else: ?>
	<p><?php echo lang('calendar:currently_no_posts');?></p>
<?php endif; ?>